## USAGE

Start by creating .env similar to .env.example. Must include all variables listed in .env.example.
Although you can include your AWS root key, a more secure alternative is to create an IAM user for
the website with ReadOnly EC2 and Cloudwatch access, provide that key information in .env file.


