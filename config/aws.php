<?php
/**
 * Created by PhpStorm.
 * User: ottov
 * Date: 12/20/16
 * Time: 3:29 PM
 */
use Aws\CloudWatch\CloudWatchClient;
use Aws\Ec2\Ec2Client;
use Aws\Batch\BatchClient;

$container = $app->getContainer();

$container["aws_cw"] = function($container){
    // Instantiate with your AWS credentials
    return new CloudWatchClient(array(
        'region'      => getenv('AWS_REGION'),
        'version'     => '2010-08-01',
        'credentials' => array(
            'key'    => getenv('AWS_ACCESS_KEY_ID'),
            'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
        )
    ));
};

$container["aws_ec2"] = function($container) {
    return new Ec2Client(array(
        'region' => getenv('AWS_REGION'),
        'version' => '2016-11-15',
        'credentials' => array(
            'key' => getenv('AWS_ACCESS_KEY_ID'),
            'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
        )
    ));
};

$container["aws_batch"] = function($container) {
    return new BatchClient(array(
        'region' => getenv('AWS_REGION'),
        'version' => '2016-08-10',
        'credentials' => array(
            'key' => getenv('AWS_ACCESS_KEY_ID'),
            'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
        )
    ));
};
