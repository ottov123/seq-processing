<?php
$container = $app->getContainer();

$container['db'] = function ($c) {

    $pdo = new PDO("mysql:host=" . getenv("DB_HOST") . ";dbname=" . getenv("DB_NAME"),
        getenv("DB_USER"), getenv("DB_PASSWORD"), array(
          PDO::ATTR_PERSISTENT => false
        )
          );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_TIMEOUT, 20);
    return $pdo;
};

