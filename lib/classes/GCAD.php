<?php

/**
 * User: otto
 * Date: 12/17/16
 * https://github.com/slimphp/Tutorial-First-Application/blob/master/src/public/index.php
 * https://github.com/slimphp/Slim-Skeleton/blob/master/src/dependencies.php
 * https://github.com/tuupola/slim-api-skeleton
 * https://www.slimframework.com/docs/objects/request.html#route-object
 */

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

abstract class GCAD
{
    /**
     * @var \Interop\Container\ContainerInterface
     */
    protected $container;

    /**
     * Controller Contructor.
     *
     * @param \Interop\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

    }

    /**
     * Render a view.
     *
     * @param ResponseInterface $response
     * @param string            $view
     * @param array             $data
     * @return ResponseInterface
     */
    protected function render(ResponseInterface $response, $view, $data = [])
    {
        /* @var $renderer \Slim\Views\PhpRenderer */
        //$renderer = $this->container->get('renderer');
        //$templateFinder = $this->container->get('templateFinder');
        //return $renderer->render($response, $templateFinder($view), $data);
        return $this->view->render($response, $templateFinder($view), $data);
    }
}

