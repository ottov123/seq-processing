<?php
  /**
   * Created by PhpStorm.
   * User: otto
   * Date: 2/16/19
   * Time: 8:09 PM
   */


  use AWS\Exception\AwsException;
  class GCAD_jobs  extends GCADtracking {
    public $RUN_ID;

    public function submitJob(string $job_name, int $cpu, int $memory, $parameters, $environment, $depends=null, int $array_size=null): array {
      // aws-batch job names can not contain '.'
      $job_name = str_replace('.', '_', $job_name);

      $arguments = [
        'jobName' => $job_name,             // REQUIRED
        'jobQueue' => 'pipeline-queue',     // REQUIRED
        'jobDefinition' => 'pipeline-01:3', // REQUIRED
        'containerOverrides' => [
          // 'command' => ['echo abcd'],
          'environment' => [
            ['name' => 'ENABLE_DB', 'value' => '1'],
            ['name' => 'ENABLE_PUSHOVER_API', 'value' => '0'],
            ['name' => 'RID', 'value' => $this->RUN_ID],
          ],
          'memory' => $memory,
          'vcpus' => $cpu,
           //'instanceType' => '<string>',
        ],
        //'dependsOn' => [
        //    [
        //        'jobId' => '<string>',
        //        'type' => 'SEQUENTIAL',
        //    ],
        //],
        'parameters' => $parameters,
        'timeout' => ['attemptDurationSeconds' => 43200],
        'retryStrategy' => ['attempts' => 2], //RetryAttempts must be between 1 and 10
      ];

      if ($environment){

        foreach ($environment as $env) {
          array_push($arguments['containerOverrides']['environment'], $env);
        }
      }

      if ($depends){
        $arguments['dependsOn'] = $depends;
      }

      if($array_size){
        $arguments['arrayProperties'] = ['size' => $array_size  ];
      }

      try {
        $res = $this->container->aws_batch->submitJob($arguments);
      } catch (AwsException $e) {
        throw $e;
      }
      //print_r($res);
      $jobID = $res->get('jobId');
      $status = $res['@metadata']['statusCode'];

      return [$job_name, $jobID, $status];

    }

  }
