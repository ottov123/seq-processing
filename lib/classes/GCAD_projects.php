<?php
  use Slim\Http\Request;
  use Slim\Http\Response;

  class GCAD_projects extends GCADtracking {
      /**
       * /v1/projects
       * @param Request $request
       * @param Response $response
       * @param $args
       * @return Response
       */
    public function listProjects(Request $request, Response $response, $args) {
      $this->container->logger->addInfo("Listing projects");

      $data = $request->getQueryParams();

      $sql = "SELECT *
              FROM seq_projects p
             ";

      $stmt = $this->container->db->query($sql);

      // default print to HTML
      if (isset($data[ 'rettype' ])) {
        if ($data[ 'rettype' ] == "json") {
          $result = $stmt->fetchAll();
          return $response->withJson($result);
        }
      }

      $response = $this->container->view->render($response, "viewQuery_as_table-sorter.phtml", ["stmt" => $stmt]);

      return $response;
    }

      /**
       * /v1/projects/{name}
       * @param Request $request
       * @param Response $response
       * @param $args
       * @return Response
       * @throws Exception
       */
    public function lookupProject(Request $request, Response $response, $args) {
      if (!(isset($args['name']))){
        throw new Exception("could not lookup project");
      }
      $name = filter_var($args['name'], FILTER_SANITIZE_STRING);

      $sql = "SELECT *
              FROM seq_projects p
              WHERE p.project_name=:project_name
             ";

      $stmt = $this->container->db->prepare($sql);

      try {

        $result = $stmt->execute(array(
            ":project_name" => $name
          )
        );
        $row = $stmt->fetch();

        if ($row['project_id']){
          $json = array('status' => 'success', 'project_id' => $row['project_id'] );
        } else {
          // no result
          $json = array('status' => 'failed' );
        }
        return $response->withJson($json);

      } catch( PDOException $Exception) {
        $this->container->logger->addInfo("Failed project lookup");
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
        return $response->withJson($json);
      }
    }

      /**
       * linked to route /v1/projects/add
       * @param Request $request
       * @param Response $response
       * @param $args
       * @return Response
       */
    public function addProject(Request $request, Response $response, $args) {
      $this->container->logger->addInfo("Adding a project");
      $data = $request->getParsedBody();
      $project_data = [];

      // Required
      if (isset($data['project_name'])){
        $project_data['project_name'] = filter_var($data['project_name'], FILTER_SANITIZE_STRING);
      } else {
        $this->container->logger->addInfo("Adding a project failed");
        return $response->withJson(['invalid']);
      }

      // Optional
      if (isset($data['project_name_full'])){
        $project_data['project_name_full'] = filter_var($data['project_name_full'], FILTER_SANITIZE_STRING);
      }else{
        $project_data['project_name_full'] = NULL;
      }

      if (isset($data['project_desc'])){
        $project_data['project_desc'] = filter_var($data['project_desc'], FILTER_SANITIZE_STRING);
      }else{
        $project_data['project_desc'] = NULL;
      }

      if (isset($data['project_center'])){
        $project_data['project_center'] = filter_var($data['project_center'], FILTER_SANITIZE_STRING);
      }else{
        $project_data['project_center'] = NULL;
      }

      if (isset($data['subproject_name'])){
        $project_data['subproject_name'] = filter_var($data['subproject_name'], FILTER_SANITIZE_STRING);
      }else{
        $project_data['subproject_name'] = NULL;
      }


      $sql = "INSERT INTO seq_projects
            (project_name, project_name_full, project_desc, project_center, subproject_name) VALUES
            (:project_name, :project_name_full, :project_desc, :project_center, :subproject_name
            )";
      $stmt = $this->container->db->prepare($sql);

      try {

        $result = $stmt->execute($project_data);
        $json = array('status' => 'success', 'project_id' => $this->container->db->lastInsertId() );
        return $response->withJson($json);

      } catch( PDOException $Exception) {
        $this->container->logger->addInfo("Adding a project error: " . $Exception->getMessage());
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
        return $response->withJson($json);
      }
    }

      /**
       * linked to route /v1/projects/sample/add
       * @param Request $request
       * @param Response $response
       * @param $args
       * @return Response
       */
    public function addProjectSample(Request $request, Response $response, $args){
      $data = $request->getParsedBody();
      $sample_data = [];

      // Required vars
      if (isset($data['project_id'])){
        $sample_data['project_id'] = filter_var($data['project_id'], FILTER_SANITIZE_NUMBER_INT);
      } else {
        $this->container->logger->addInfo("Adding a sample failed");
        return $response->withJson(array('status'=>'failed','reason'=>'insert failed, missing project_id'));
      }

      if (isset($data['sample_name'])){
        $sample_data['sample_name'] = filter_var($data['sample_name'], FILTER_SANITIZE_STRING);
      } else {
        $this->container->logger->addInfo("Adding a sample failed");
        return $response->withJson(array('status'=>'failed','reason'=>'insert failed, missing sample_name'));
      }

      $sample_data['subject_name'] = (isset($data['subject_name'])? filter_var($data['subject_name'],FILTER_SANITIZE_STRING):NULL);
      $sample_data['LSAC'] = (isset($data['LSAC'])? filter_var($data['LSAC'],FILTER_SANITIZE_STRING):NULL);
      $sample_data['seq_type'] = (isset($data['seq_type'])? filter_var($data['seq_type'],FILTER_SANITIZE_STRING):NULL);

      $sql = "INSERT INTO seq_samples
            (project_id, sample_name, subject_name, LSAC, seq_type) VALUES
            (:project_id,:sample_name,:subject_name,:LSAC,:seq_type
            )";
      $stmt = $this->container->db->prepare($sql);

      try {

        $result = $stmt->execute($sample_data);
        $json = array('status' => 'success', 'sample_id' => $this->container->db->lastInsertId() );
        return $response->withJson($json);

      } catch( PDOException $Exception) {
        $this->container->logger->addInfo("Adding a sample error:" . $Exception->getMessage() );
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
        return $response->withJson($json);
      }
    }

      /**
       * linked to route /v1/projects/samples/by-project-id/{id}
       * @param Request $request
       * @param Response $response
       * @param $args
       * @return array|Response
       */
    public function getSamplesByPid(Request $request, Response $response, $args){
      $data = $request->getQueryParams();

      $id = filter_var($args['id'], FILTER_SANITIZE_STRING);
      $sql = "SELECT *
              FROM seq_samples
              WHERE project_id = :id
             ";

      $count = $this->validateProjectID($id);
      if ($count != 1) { return jsonFormatMsg("project_id not valid");}

      $stmt = $this->container->db->prepare($sql);

      //$count = $stmt->rowCount();

      try {
        $stmt->execute(array(
            ":id" => $id
          )
        );

        // default print to HTML
        if (isset($data['rettype'])){
          if ($data['rettype'] == "json"){
            $result = $stmt->fetchAll();
            return $response->withJson($result);
          }
        }

        $response = $this->container->view->render($response, "viewQuery_as_table-sorter.phtml", ["stmt" => $stmt]);

      } catch( PDOException $Exception) {
        $this->container->logger->addInfo("Failed project-sample lookup by id");
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(), 'errCode' => $Exception->getCode() );
        return $response->withJson($json);
      }
      return $response;
    }
  }
