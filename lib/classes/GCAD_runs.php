<?php

  use Slim\Http\Request;
  use Slim\Http\Response;

  class GCAD_runs extends GCADtracking {
      public function viewRuns(Request $request, Response $response, $args)
      {

          $q = $request->getQueryParams();
          $pid = filter_var($q[ 'project_id' ], FILTER_SANITIZE_NUMBER_INT);

          if (empty($q[ 'project_id' ])) {
              print "missing project_id";
              exit();
          }else{
              $sql="SELECT 
                        SR.sample_id, SS.sample_name, COUNT(SR.ID) AS 'Run Count'
                    FROM
                        seq_processing.seq_runs AS SR
                            INNER JOIN
                        seq_samples AS SS ON SR.sample_id = SS.sample_id
                            AND SR.project_id = SS.project_id
                    WHERE
                        SR.project_id = $pid
                    GROUP BY SR.sample_id, SS.sample_name
                    ORDER BY 'Run Count' DESC";
          }

          if (!empty($q[ 'sample_id' ])) {
              $sid = filter_var($q[ 'sample_id' ], FILTER_SANITIZE_NUMBER_INT);


              $sql="SELECT 
                        SR.sample_id, SS.sample_name, SR.ID AS 'RunID'
                    FROM
                        seq_processing.seq_runs AS SR
                            INNER JOIN
                        seq_samples AS SS ON SR.sample_id = SS.sample_id
                            AND SR.project_id = SS.project_id
                    WHERE
                        SR.project_id = $pid
                        AND SR.sample_id = $sid
                    ORDER BY SR.ID DESC";
          }



          $count = $this->validateProjectID($pid);

          if ($count != 1) { return jsonFailMsg($response, "project_id not valid");}




          try {
              $stmt = $this->container->db->query($sql);
              $response = $this->container->view->render($response, "viewQuery_as_table-sorter-links.phtml", [
                  "stmt" => $stmt,
                  "href" => array("Run Count","/v1/runs?project_id=$pid&sample_id=##sample_id##", "sample_id"),
              ]);
              unset($stmt);
              return $response;
          } catch (Exception $Exception) {
              $this->container->logger->addInfo("Failed table build");
              $json = array('status'  => 'failed',
                  'message' => $Exception->getMessage(),
                  'errCode' => $Exception->getCode()
              );
          }
          return $response->withJson($json);
      }

      public function viewRun(Request $request, Response $response, $args)
      {
          $q = $request->getQueryParams("run_id");
          if (empty($q[ 'run_id' ])) {
              print "missing run_id";
              exit();
          }

          $pid = filter_var($q[ 'project_id' ], FILTER_SANITIZE_NUMBER_INT);

          $count = $this->validateRunID($pid);

          if ($count != 1) { return jsonFailMsg($response, "project_id not valid");}

          $sql="SELECT 
                    SR.sample_id, SS.sample_name, COUNT(SR.ID) AS 'Run Count'
                FROM
                    seq_processing.seq_runs AS SR
                        INNER JOIN
                    seq_samples AS SS ON SR.sample_id = SS.sample_id
                        AND SR.project_id = SS.project_id
                WHERE
                    SR.project_id = $pid
                GROUP BY SR.sample_id, SS.sample_name
                ORDER BY 'Run Count' DESC";

          $stmt = $this->container->db->prepare($sql);
          try {
              $stmt = $this->container->db->query($sql);
              $response = $this->container->view->render($response, "viewQuery_as_table-sorter.phtml", ["stmt" => $stmt]);
              unset($stmt);
              return $response;
          } catch (Exception $Exception) {
              $this->container->logger->addInfo("Failed table build");
              $json = array('status'  => 'failed',
                  'message' => $Exception->getMessage(),
                  'errCode' => $Exception->getCode()
              );
          }
          return $response->withJson($json);
      }

  }
