<?php

use Slim\Http\Request;
use Slim\Http\Response;

class GCAD_samples extends GCADtracking
{
    public function validateSampleTrackingName(string $tracking_name)
    {
        // Validate tracking name
        $sql_track = "SELECT sample_attribute_id
                  FROM seq_sample_attributes
                  WHERE attribute_name = :tracking_name
                  ";
        $stmt = $this->container->db->prepare($sql_track);
        $stmt->execute(array(":tracking_name" => $tracking_name));
        //$count = $stmt->rowCount();
        //if ($count < 1){ jsonFailMsg($response, "tracking_name not found");return;}
        $result = $stmt->fetch();
        return $result['sample_attribute_id'];
    }

    // Extract attr_type to determine tbl name
    public function getSampleTblType(int $tracking_id)
    {
        $sql_track = "SELECT attribute_type
                      FROM seq_sample_attributes
                      WHERE sample_attribute_id = :tracking_id
                      ";
        $stmt = $this->container->db->prepare($sql_track);
        $stmt->execute(array(":tracking_id" => $tracking_id));
        $row = $stmt->fetch();
        return $row['attribute_type'];
    }

    // linked to route for /v1/sample/sample-name/{name}
    public function getSampleIdByName(Request $request, Response $response, $args)
    {

        $name = filter_var($args['name'], FILTER_SANITIZE_STRING);

        $sql = "SELECT *
            FROM  seq_samples AS S
            WHERE sample_name=:sample_name
           ";

        $stmt = $this->container->db->prepare($sql);

        try {

            $result = $stmt->execute(array(
                    ":sample_name" => $name
                )
            );
            $list = $stmt->fetchAll();
            return $response->withJson($list);

        } catch (PDOException $Exception) {
            $this->container->logger->addInfo("Failed sample lookup");
            $json = array('status' => 'failed', 'message' => $Exception->getMessage(), 'errCode' => $Exception->getCode());
            return $response->withJson($json);
        }
    }

    /**
     * SETTER: linked to route for /v1/sample/set-attr/{attr_name}/{project_id}/{sample_name}/{value}
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed|Response
     */
    public function setSampleAttribute(Request $request, Response $response, $args)
    {
        if (count($args) < 4) {
            return jsonFailMsg($response, "callback requires project_id, sample_name, attribute_name, and value");
        }

        $attr_name = filter_var($args['attr_name'], FILTER_SANITIZE_STRING);
        $project_id = filter_var($args['project_id'], FILTER_SANITIZE_NUMBER_INT);
        $sample_name = filter_var($args['sample_name'], FILTER_SANITIZE_STRING);
        $value = rawurldecode(filter_var($args['value'], FILTER_SANITIZE_ENCODED));

        // Validate sample_name
        $sample_id = $this->validateSampleName($sample_name, $project_id);
        if (empty($sample_id)) {
            return jsonFailMsg($response, "sample_name not found");
        }

        // Validate tracking name
        $attr_id = $this->validateSampleTrackingName($attr_name);
        if (empty($attr_id)) {
            return jsonFailMsg($response, "tracking_name not found");
        }

        $tbl_type = $this->getSampleTblType($attr_id);

        $sql = "INSERT INTO
            seq_sample_attributes_$tbl_type
            (project_id, sample_id, attr_id, sample_attr_value) VALUES
            (:project_id, :sample_id, :attr_id, :val)
            ON DUPLICATE KEY UPDATE sample_attr_value = :val
           ";
        $stmt = $this->container->db->prepare($sql);

        try {

            $result = $stmt->execute(
                array(
                    ':project_id' => $project_id,
                    ':sample_id' => $sample_id,
                    ':attr_id' => $attr_id,
                    ':val' => $value)
            );
            $json = array('status' => 'success', 'id' => $this->container->db->lastInsertId());
            return $response->withJson($json);

        } catch (PDOException $Exception) {

            if ($Exception->getCode() == 23000) {
                $json = array('status' => 'failed', 'message' => 'Duplicate', 'errCode' => $Exception->getCode());
                return $response->withJson($json);
            }
            $this->container->logger->addInfo("Adding a attribute value error:" . $Exception->getMessage());
            $json = array('status' => 'failed', 'message' => $Exception->getMessage(), 'errCode' => $Exception->getCode());
            return $response->withJson($json);
        }
    }

    /**
     * SETTER: linked to route for /v1/sample/set-attr-by-id/{attr_name}/{project_id}/{sample_id}/{value}
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed|Response
     */
    public function setSampleAttributeById(Request $request, Response $response, $args)
    {
        if (count($args) <> 4) {
            return jsonFailMsg($response, "callback requires project_id, sample_id, attribute_name, and value");
        }

        $attr_name = filter_var($args['attr_name'], FILTER_SANITIZE_STRING);
        $project_id = filter_var($args['project_id'], FILTER_SANITIZE_NUMBER_INT);
        $sample_id = filter_var($args['sample_id'], FILTER_SANITIZE_NUMBER_INT);
        $value = rawurldecode(filter_var($args['value'], FILTER_SANITIZE_ENCODED));

        // Validate sample_id
        if ($this->validateSampleID($sample_id)<1) {
            return jsonFailMsg($response, "sample_id not found");
        }

        // Validate tracking name
        $attr_id = $this->validateSampleTrackingName($attr_name);
        if (empty($attr_id)) {
            return jsonFailMsg($response, "tracking_name not found");
        }

        $tbl_type = $this->getSampleTblType($attr_id);

        $sql = "INSERT INTO
            seq_sample_attributes_$tbl_type
            (project_id, sample_id, attr_id, sample_attr_value) VALUES
            (:project_id, :sample_id, :attr_id, :val)
            ON DUPLICATE KEY UPDATE sample_attr_value = :val
           ";
        $stmt = $this->container->db->prepare($sql);

        try {

            $result = $stmt->execute(
                array(
                    ':project_id' => $project_id,
                    ':sample_id' => $sample_id,
                    ':attr_id' => $attr_id,
                    ':val' => $value)
            );
            $json = array('status' => 'success', 'id' => $this->container->db->lastInsertId());
            return $response->withJson($json);

        } catch (PDOException $Exception) {

            if ($Exception->getCode() == 23000) {
                $json = array('status' => 'failed', 'message' => 'Duplicate', 'errCode' => $Exception->getCode());
                return $response->withJson($json);
            }
            $this->container->logger->addInfo("Adding a attribute value error:" . $Exception->getMessage());
            $json = array('status' => 'failed', 'message' => $Exception->getMessage(), 'errCode' => $Exception->getCode());
            return $response->withJson($json);
        }
    }

    /**
     * GETTER: linked to route for /v1/sample/get-attr/{attr_name}/{project_id}/{sample_name}
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed|Response
     */
    public function getSampleAttribute(Request $request, Response $response, $args)
    {
        if (count($args) <> 3) {
            jsonFailMsg($response, "callback requires project_id, sample_name, and attribute_name");
        }

        $attr_name = filter_var($args['attr_name'], FILTER_SANITIZE_STRING);
        $project_id = filter_var($args['project_id'], FILTER_SANITIZE_NUMBER_INT);
        $sample_name = filter_var($args['sample_name'], FILTER_SANITIZE_STRING);

        // Validate project_id
        $count = $this->validateProjectID($project_id);
        if ($count != 1) {
            return jsonFormatMsg("project_id not valid");
        }

        // Validate sample_name
        $sample_id = $this->validateSampleName($sample_name, $project_id);
        if (empty($sample_id)) {
            return jsonFailMsg($response, "sample_name not found");
        }

        // Validate tracking name
        $attr_id = $this->validateSampleTrackingName($attr_name);
        if (empty($attr_id)) {
            return jsonFailMsg($response, "tracking_name ($attr_name) not found");
        }

        $tbl_type = $this->getSampleTblType($attr_id);

        $sql = "SELECT sample_attr_value
            FROM seq_sample_attributes_$tbl_type
            WHERE project_id = :project_id
              AND sample_id  = :sample_id
              AND attr_id    = :attr_id
           ";
        $stmt = $this->container->db->prepare($sql);
        $stmt->execute(array(
            ":project_id" => $project_id,
            ":sample_id" => $sample_id,
            ":attr_id" => $attr_id
        ));
        $result = $stmt->fetch();
        $json = array('status' => 'success', 'value' => $result['sample_attr_value']);
        return $response->withJson($json);
    }

    /**
     * SETTER: linked to route for /v1/sample/define-target
     *
     */
    public function defineTargetFileLocation(Request $request, Response $response, $args){
        $this->container->logger->addInfo("Adding a WES target file");
        $data = $request->getParsedBody();
        $trgt_data = [];

        // Required
        if (isset($data['file_title'])){
          $trgt_data['file_title'] = filter_var($data['file_title'], FILTER_SANITIZE_STRING);
        } else {
          $this->container->logger->addWarning("Adding a WES target failed");
          return $response->withJson(['invalid']);
        }

        // Required
        if (isset($data['s3_path'])){
          $trgt_data['s3_path'] = filter_var($data['s3_path'], FILTER_SANITIZE_URL);
        } else {
          $this->container->logger->addWarning("Adding a WES target failed");
          return $response->withJson(['invalid']);
        }

        // Optional
        if (isset($data['desc'])){
          $trgt_data['desc'] = filter_var($data['desc'], FILTER_SANITIZE_STRING);
        } else {
          $trgt_data['desc'] = NULL;
        }

        $sql = "INSERT INTO seq_wes_interval_files
              (`name`, `desc`, `s3_path`) VALUES
              (:file_title, :desc, :s3_path
              )";
        $stmt = $this->container->db->prepare($sql);

        try {

          $result = $stmt->execute($trgt_data);
          $json = array('status' => 'success', 'file_id' => $this->container->db->lastInsertId() );
          return $response->withJson($json);

        } catch( PDOException $Exception) {
          $this->container->logger->addError("Adding a WES target error: " . $Exception->getMessage());
          $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
          return $response->withJson($json);
        }

    }

    /**
     * GETTER: linked to route for /v1/sample/get-target/{project_id}/{sample_name}
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return json
     */
    public function getTargetFileLocation(Request $request, Response $response, $args)
    {
        if (count($args) <> 2) {
            jsonFailMsg($response, "callback requires project_id, sample_name");
        }

        $project_id = filter_var($args['project_id'], FILTER_SANITIZE_NUMBER_INT);
        $sample_name = filter_var($args['sample_name'], FILTER_SANITIZE_STRING);

        // Validate project_id
        $count = $this->validateProjectID($project_id);
        if ($count != 1) {
            return jsonFormatMsg("project_id not valid");
        }

        // Validate sample_name
        $sample_id = $this->validateSampleName($sample_name, $project_id);
        if (empty($sample_id)) {
            return jsonFailMsg($response, "sample_name not found");
        }

        // targets_id -> 16

        $tbl_type = $this->getSampleTblType(16);

        $sql = "SELECT sample_attr_value
            FROM seq_sample_attributes_$tbl_type
            WHERE project_id = :project_id
              AND sample_id  = :sample_id
              AND attr_id    = 16
           ";
        $stmt = $this->container->db->prepare($sql);
        $stmt->execute(array(
            ":project_id" => $project_id,
            ":sample_id" => $sample_id,
        ));
        $result = $stmt->fetch();

        $sql = "SELECT `s3_path`
            FROM seq_wes_interval_files
            WHERE `ID` = :tid
           ";
        $stmt = $this->container->db->prepare($sql);
        $stmt->execute(array(
            ":tid" => $result['sample_attr_value']
        ));
        $result = $stmt->fetch();



        $json = array('status' => 'success', 'value' => $result['s3_path']);
        return $response->withJson($json);
    }

    /**
     * linked to route /v1/sample/details
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function getSampleDetails(Request $request, Response $response, $args)
    {

        $q = $request->getQueryParams("project_id");
        if (empty($q[ 'project_id' ])) {
            print "missing project_id";
            exit();
        }

        $pid = filter_var($q[ 'project_id' ], FILTER_SANITIZE_NUMBER_INT);

        $count = $this->validateProjectID($pid);
        if ($count != 1) { return jsonFailMsg($response, "project_id not valid");}

        # Pagination
        $page      = ($request->getParam('page', 0) > 0) ? $request->getParam('page') : 1;
        $limit     = $request->getParam('limit', 0); // Number of rows on one page
        $skip      = ($page - 1) * $limit;

        $sql0 = $this->getSampleAttributesSql($pid,null,null);
        $sql  = $this->getSampleAttributesSql($pid,null,null, $limit, $skip);

        try {
            $stmt = $this->container->db->query($sql);

            if (isset($q['rettype'])) {
                if ($q['rettype'] == "json") {
                    $result = $stmt->fetchAll();

                    # Get true size of results for pagination
                    $stmt0 = $this->container->db->query($sql0);
                    $sz    = $stmt0->rowCount();

                    return $response->withJson(array("data"=>$result,"data_size"=>$sz) );
                }
            }

            $response = $this->container->view->render($response, "viewQuery_as_table-sorter.phtml", ["stmt" => $stmt]);
            unset($stmt);
            return $response;
        } catch (Exception $Exception) {
            $this->container->logger->addInfo("Failed table build");
            $json = array('status'  => 'failed',
                'message' => $Exception->getMessage(),
                'errCode' => $Exception->getCode()
            );
        }
        return $response->withJson($json);
    }


    /**
     * @param int $pid
     * @param string|null $whereExtra
     * @param string|null $orderbyExtra
     * @param int $limit
     * @param int $skip
     * @return string
     */
    public function getSampleAttributesSql(int $pid, string $whereExtra = null, string $orderByExtra = null, int $limit = 0, int $skip=0)
    {
        // Gather column metadata for stage
        $sql = "SELECT `sample_attribute_id`,`attribute_name`,`attribute_type`,`attribute_desc`
                  FROM seq_sample_attributes
                  WHERE `disabled`  IS NULL OR `disabled` != 1
                  ORDER BY sample_attribute_id
                 ";

        $stmt = $this->container->db->prepare($sql);

        try {

            $result = $stmt->execute();
            $list = $stmt->fetchAll();

            $select = "SELECT
                SM.project_id,
                SM.sample_name AS sample_name,
                SM.LSAC AS LSAC";

            $from = " FROM seq_samples SM ";

            $ct = 0;
            foreach ($list as $rw) {
                $alias = "SA" . $ct;
                $select .= ", ${alias}.sample_attr_value AS `" . $rw['attribute_name'] . "`";
                $from .= " LEFT JOIN seq_sample_attributes_" . $rw['attribute_type'] . " $alias
                 ON ${alias}.project_id=SM.project_id AND ${alias}.sample_id=SM.sample_id
                 AND ${alias}.attr_id=" . $rw['sample_attribute_id'];

                $ct++;
            }

            $where = " WHERE SM.project_id = $pid " . ( isset($whereExtra)? ',' . $whereExtra:"" );
            $orderBy = " ORDER BY SM.LSAC, SM.sample_name " . ( isset($orderByExtra)? ',' . $orderByExtra:"" );
            $limitBy = " ";
            if ($limit > 0){
                $limitBy = " LIMIT $limit OFFSET $skip ";
            }

            $sql = $select . $from . $where . $orderBy . $limitBy;

            //$response->getBody()->write($sql);
            unset($list);
            unset($stmt);

            return $sql;

        } catch (PDOException $Exception) {
            return "SELECT `" . $Exception->getMessage() . "`";
            //    $this->logger->addInfo("Failed table build");
            //    $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
        } catch (Exception $Exception) {
            return "SELECT `" . $Exception->getMessage() . "`";
            //    $this->logger->addInfo("Failed table build");
            //    $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );


        }

    }
}
