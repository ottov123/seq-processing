<?php

  use Slim\Http\Request;
  use Slim\Http\Response;

  class GCAD_stages extends GCADtracking {
   /**
     * viewStage action.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function viewStage(Request $request, Response $response, $args) {

      $q = $request->getQueryParams();
      if (empty($q[ 'project_id' ])) {
        print "missing project_id";
        exit();
      }

      $rid = null;
      if (isset($q[ 'run_id' ])) {
          $rid = filter_var($q[ 'run_id' ], FILTER_SANITIZE_NUMBER_INT);
      }

      $pid = filter_var($q[ 'project_id' ], FILTER_SANITIZE_NUMBER_INT);

      $stageID = filter_var($args['stage'], FILTER_SANITIZE_STRING);
      $stageID = str_replace("stage", "", $stageID);

      if (!array_search($stageID, array(1=>"0", "1", "2a", "2b", "2b-g4"))){
        print "wrong stage ID";
        exit();
      }

      $count = $this->validateProjectID($pid);
      if ($count != 1) { return jsonFailMsg($response, "project_id not valid");}

      $sql = $this->getStageAttributesSql('stage ' . $stageID, $pid,null , null, $rid);


      try {
        $stmt = $this->container->db->query($sql);

         if (isset($q['rettype'])) {
             if ($q['rettype'] == "json") {
                 $result = $stmt->fetchAll();

                 # Get true size of results for pagination
                 $stmt0 = $this->container->db->query($sql);
                 $sz    = $stmt0->rowCount();

                 return $response->withJson(array("data"=>$result,"data_size"=>$sz) );
             }
         }


        $response = $this->container->view->render($response, "viewQuery_as_table-sorter.phtml", ["stmt" => $stmt]);
        unset($stmt);
        return $response;
      } catch (Exception $Exception) {
        $this->container->logger->addInfo("Failed table build");
        $json = array('status'  => 'failed',
                      'message' => $Exception->getMessage(),
                      'errCode' => $Exception->getCode()
        );
      }
      return $response->withJson($json);
    }

      /**
       * @param string $stage
       * @param int $pid
       * @param string|null $whereExtra
       * @param string|null $orderbyExtra
       * @param int|null $rid
       * @return string
       */
      public function getStageAttributesSql(string $stage, int $pid, string $whereExtra=null, string $orderbyExtra=null, int $rid = null){
      // Gather column metadata for stage
      $sql = "SELECT track_step_id, step_name_full, step_type, step_desc
              FROM seq_tracking_processing_steps
              WHERE step_phase_name=:stage AND `disabled` = 0
              ORDER BY col_index
             ";

      $stmt = $this->container->db->prepare($sql);

      try {

        $result = $stmt->execute(
          array(":stage" => $stage)
        );
        $list = $stmt->fetchAll();
        //$response->getBody()->write("<pre>".var_export($list,true) ."</pre>");

        /*1 =>  array ('track_step_id' => '12', 'step_name_full' => 'picard_sam_sort','step_type' => 'BOOL', 'step_desc' => 'if the bam is s$
        */
        // Use column info to build multi left join query
        // pretty simple when you iterate
        //
        $select = "SELECT
        SM.sample_name AS sample_name,
        SM.LSAC AS LSAC,
        SM_ATTR.sample_attr_value AS RunID
        ";

        //$from = " FROM seq_samples SM ";
        $from = " FROM seq_samples SM LEFT JOIN
                        seq_sample_attributes_INT SM_ATTR ON 
                        SM_ATTR.sample_id = SM.sample_id 
                        AND SM_ATTR.project_id = SM.project_id 
                        AND SM_ATTR.attr_id = 15
                        ";
        if (is_null($rid)) {
            $ridVAR = "SM_ATTR.sample_attr_value";
        }else{
            $ridVAR = $rid;
        }

        $ct=0;
        foreach ($list as $rw){
          $alias = "ST" . $ct;
          $select .= ", ${alias}.tracking_value AS `" . $rw['step_name_full'] . "`";
//          $from .= " LEFT JOIN seq_tracking_processing_steps_" . $rw['step_type']  . " $alias
//                 ON ${alias}.project_id=SM.project_id
//                 AND ${alias}.sample_id=SM.sample_id
//                 AND ${alias}.tracking_step_id=" . $rw['track_step_id'];
          $from .= " LEFT JOIN seq_tracking_processing_steps_" . $rw['step_type']  . " $alias
                 ON ${alias}.run_id=$ridVAR
                 AND ${alias}.tracking_step_id=" . $rw['track_step_id'];

          $ct++;
        }

        $where=" WHERE SM.isDropped=0 AND SM.project_id = $pid " . ( isset($whereExtra)? ',' . $whereExtra:"" );
        $orderby="ORDER BY SM.LSAC, SM.sample_name " . ( isset($orderbyExtra)? ',' . $orderbyExtra:"" );
        $sql = $select . $from . $where . $orderby;

        //$response->getBody()->write($sql);
        unset($list);
        unset($stmt);

        return $sql;

      } catch( PDOException $Exception) {
        return ("SELECT `" . $Exception->getMessage() . "`");
        //    $this->logger->addInfo("Failed table build");
        //    $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
      } catch (Exception $Exception){
        return ("SELECT `" . $Exception->getMessage() . "`");
        //    $this->logger->addInfo("Failed table build");
        //    $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );


      }

    }
  }
