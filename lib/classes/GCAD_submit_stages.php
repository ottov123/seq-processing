<?php
  /**
   * Created by PhpStorm.
   * User: otto
   * Date: 2/16/19
   * Time: 8:20 PM
   */
  use Slim\Http\Request;
  use Slim\Http\Response;

  class GCAD_submit_stages  extends GCAD_jobs {

    public $SM;     // Sample Name
    public $PRJ_ID; // Project ID
    public $rpath;  // Results Path - cloud directory
    public $sz;     // Source file(s) size, in GB
    public $RN;     // Random string for job names
    public $WES_target;     // is WES sample

    public $RETRY_STRATEGY;

    // /v1/batch-submit/$SM/$PRJ_ID
    public function batch_submit (Request $request, Response $response, $args) {

        $data = $request->getParsedBody();// POST ARGS
        if (!empty($data)){
            $args=[];
            $args['SM'] = $data['SM'];
            $args['PRJ_ID'] = $data['PRJ_ID'];
        }

        //print_r($args);

        $params = $request->getQueryParams();
        $skip_to = NULL;
        $skip_stage0 = false;
        $skip_stage1 = false;
        $skip_stage2a = false;
        $skip_stage2b = false;
        if (!empty($params)){
            //print_r($params);
            if (isset($params['skip_to'])){
                $skip_to = filter_var($params['skip_to'], FILTER_SANITIZE_STRING);

                if ($skip_to != 'stage2b-g4'){
                    print("skip_to setting is unsupported");
                    return false;
                }

                $skip_stage0 = true;
                $skip_stage1 = true;
                $skip_stage2a = true;
                $skip_stage2b = false;
            }

        }



        $this->SM = filter_var($args['SM'], FILTER_SANITIZE_STRING);
        $this->PRJ_ID = filter_var($args['PRJ_ID'], FILTER_SANITIZE_NUMBER_INT);
        $this->RETRY_STRATEGY="replace";//{skip, replace, NC}

        if (!$this->validateProjectID($this->PRJ_ID)){
            print("Unknown Project_ID\n");
            return false;
        }
        if (!$this->validateSampleName($this->SM,$this->PRJ_ID)){
            print("Unknown Sample Name\n");
            return false;
        }

        print $this->SM . " " . $this->PRJ_ID . "\n";

        $sql = "SELECT 
                    ATTR1.sample_attr_value AS 'bam_path',
                    ATTR2.sample_attr_value AS 'file_size',
                    ATTR3.sample_attr_value AS 'results_path',
                    ATTR4.sample_attr_value AS 'tid',
                    TARGET_LIST.s3_path as 'target_url',
                    ATTR5.sample_attr_value AS 'latest_run_id'
                FROM
                    seq_sample_attributes_VARCHAR AS ATTR1
                        INNER JOIN
                    seq_samples AS SM ON SM.sample_id = ATTR1.sample_id
                        INNER JOIN
                    seq_sample_attributes_INT AS ATTR2 ON ATTR2.sample_id = SM.sample_id
                        INNER JOIN
                    seq_sample_attributes_VARCHAR AS ATTR3 ON ATTR3.sample_id = SM.sample_id
                        LEFT JOIN
                    seq_sample_attributes_INT AS ATTR4 ON ATTR4.sample_id = SM.sample_id AND ATTR4.attr_id = 16
                        LEFT JOIN
                    seq_sample_attributes_INT AS ATTR5 ON ATTR5.sample_id = SM.sample_id AND ATTR5.attr_id = 15
                        LEFT JOIN
                    seq_wes_interval_files AS TARGET_LIST on ATTR4.sample_attr_value = TARGET_LIST.ID
                WHERE
                    ATTR1.attr_id = 12
                    AND ATTR2.attr_id = 5
                    AND ATTR3.attr_id = 13
                    AND SM.project_id = $this->PRJ_ID
                    AND SM.sample_name = '$this->SM'
               ";

        try {
           $stmt = $this->container->db->query($sql);
        } catch (\Throwable $e){
          //print_r($e);
          $log = $e->getMessage() . "\n" . $e->getTraceAsString();
          print("<pre>");
          print("Unhandled Exception - " . $log);
          print("</pre>");

          return false;
        }
        $count = $stmt->rowCount();
        if ($count < 1) { jsonFailMsg($response, "id not valid");return true;}

        $result = $stmt->fetchAll();

        // Set variables
        $bam = urldecode($result[0]['bam_path']);
        $sz = urldecode($result[0]['file_size']);
        $this->rpath = urldecode($result[0]['results_path']);
        $this->RN = mt_rand();
        if (!empty($result[0]['target_url'])){
            $this->WES_target = urldecode($result[0]['target_url']);
            print("using TARGET FILE:=" . $this->WES_target);
        }

        if (empty($this->rpath)){
          print("missing results path");
          return false;
        }elseif (empty($bam)){
          print("missing SEQ_FILE path");
          return false;
        }

        if (is_numeric($sz)){
          // Convert bytes to GB
          $sz = ceil(($sz/1024/1024/1024));
          if ($sz < 1){
            $sz = 4;
          }

          if (substr($bam,-5,5) == '.cram') {
              $sz = ceil($sz * 4);
          }
          print("\n\$sz:=$sz\n");
          $this->sz = $sz;
        }else{
          print("Missing file size");
          return false;
        }

        print("<pre>");

        // Set run_id
        if (!empty($result[0]['latest_run_id'])){
            $this->RUN_ID = $result[0]['latest_run_id'];
        } else{
            $args['project_id']= $this->PRJ_ID;
            $args['sample_name'] = $this->SM;

            $json = $this->initRun($request, $response, $args);
            $dat = json_decode($json->getBody());

            if ($dat->status=='success'){
                $this->RUN_ID = $dat->id;

                $g = new GCAD_samples($this->container);
                $args['attr_name'] = "latest_run_id";
                $args['value'] = $this->RUN_ID;

                $json = $g->setSampleAttribute($request, $response, $args)->getBody();
                $dat = json_decode($json);
                if (!$dat->status=='success') {
                    print('fail 0');
                    return false;
                }
                //print('success');
            }else{
                print('fail 1');
                return false;
            }
        }

        if (strpos($bam,'fastq_map') !== false) {
            print("detected FASTQ run\n");
            // download fastq_map, make read group names from number of pairs, e.g. { SM.0, SM.1 ... SM.N }
            exec("/usr/local/bin/aws s3 cp $bam - | grep $this->SM | awk 'BEGIN{CT=0};{if (NR % 2){print $2\".\"CT;CT+=1}}'", $rg_lines, $err);

        }else{
            // extract read group names
            exec("/usr/local/bin/samtools view -H $bam | grep -Po '^@RG\sID:\S+' | sed 's/@RG\tID://' ", $rg_lines, $err);

        }

        if (count($rg_lines) < 1){
            print("no read groups\n");
            print_r($err);
            return false;
        }
        print("Found read Groups:");
        print_r($rg_lines);


        /*
         * SUBMIT STAGES
         */
        $ret0 = array(NULL, ""); #($job_id, $input);
        $ret1 = array(NULL,"s3://wanglab-play/intermediates/$this->PRJ_ID/$this->SM/_RID_/part5/$this->SM.sorted.dupmarked.bam");
        $ret2a = array(NULL, "$this->rpath/$this->SM.hg38.realign.bqsr.bam");
        if (!$skip_stage0) {
            $ret0 = $this->submit_stage_0($bam, $sz, $rg_lines);
        }

        if (!$skip_stage1) {
            $ret1 = $this->submit_stage_1($sz, $ret0);
        }

        if (!$skip_stage2a) {
            $ret2a = $this->submit_stage_2a($sz, $ret1);
        }


        $ret2b = $this->submit_stage_2b($sz, $ret2a);


        print("Submitted:" . date('r') . "\n");
        return true;
  }

    // STAGES
    function submit_stage_0(string $bam, int $sz, array $rg_lines) : array{

      $step3_job_ids = array();
      $step3_outputs = array();
      $step0_job_id = null;

      if (strpos($bam,'fastq_map') !== false) {
          print("Submitting FASTQ run\n");
          $ret = $this->submit_stage_0_step_0($bam);
          $step0_job_id = [array('jobId' => $ret[0])];
          $bam = $ret[1];
      }

      foreach ($rg_lines as $rg) {
        try {
          // Step 1 - read group to query-name-sorted bam
          $ret = $this->submit_stage_0_step_1($rg, $bam, $sz, $step0_job_id);

          // Step 2 - check properly paired
          if ($step0_job_id == null) {
                $ret = $this->submit_stage_0_step_2($rg, $ret[1], $ret[0]);
          }
          // Step 3 - mapping, sorting, merging
          $ret = $this->submit_stage_0_step_3($rg, $ret[1], $ret[0], $sz);

          array_push($step3_job_ids, array('jobId' => $ret[0]));
          array_push($step3_outputs, $ret[1]);
        } catch (Aws\Exception\AwsException $e) {
          //echo $e->getAwsRequestId() . "\n";
          //echo $e->getAwsErrorType() . "\n";
          //echo $e->getAwsErrorCode() . "\n";
          $this->container->logger->addError($e->getMessage());
          print($e->getMessage());
          //return $response->withStatus(512, "Unknown Error");
            exit(1);
        }
      }// end foreach $rg

      // Step 4 - merge multiple RG BAMs into a single file. Skip if only one read group
      if (count($step3_outputs) > 1) {
        $ret = $this->submit_stage_0_step_4($step3_outputs, $step3_job_ids);
      }

      return $ret;
    }

    function submit_stage_1(int $sz, array $ret0): array {


      print("Submitting STAGE 1\n");

      // Step 1 - bamUtil/Mark-dup
      $ret1 = $this->submit_stage_1_step_1($ret0[1], $ret0[0]);

      // STAGE 1 - step 2 - depthOfCoverage
      $ret2 = $this->submit_stage_1_step_2($ret1[1], $ret1[0], $sz);

      return $ret1;
    }

    function submit_stage_2a(int $sz, array $ret0): array {

      $stage2a_job_ids = array();
      $stage2a_outputs = array($ret0[1]);
      print("Submitting STAGE 2\n");

      // Step 1a - base-recalibration
      $ret1 = $this->submit_stage_2a_step_1a($ret0[1], $ret0[0], $sz);
      array_push($stage2a_job_ids, array('jobId' => $ret1[0]));
      array_push($stage2a_outputs, $ret1[1]);

      // Step 1b - RTC
      $ret2 = $this->submit_stage_2a_step_1b($ret0[1], $ret0[0], $sz);
      array_push($stage2a_job_ids, array('jobId' => $ret2[0]));
      array_push($stage2a_outputs, $ret2[1]);

      // Step 2 - indel-realignment + merge
      $ret3 = $this->submit_stage_2a_step_2($stage2a_outputs, $stage2a_job_ids, $sz);

      return $ret3;
    }

    function submit_stage_2b(int $sz, array $ret0): array {

      print("Submitting STAGE 2\n");

      // Step 1 - Haplotype Caller (GATK 3)
 //     $ret1 = $this->submit_stage_2b_step_1($ret0[1], $ret0[0], $sz);

      // Step 2 - VariantEval
//      $input = str_replace("_INDEX_CHR_", "chr3", $ret1[1]);
//      $ret2 = $this->submit_stage_2b_step_2($input . ".tbi", $ret1[0], $sz);

      // ** GATK 4.1 ** //
      // Step 1-g4 - Haplotype Caller - GATK 4.1
      $ret1a = $this->submit_stage_2b_step_1_g4($ret0[1], $ret0[0], $sz);

      // Step 2 - VariantEval (GATK 3) - GATK - 4.1
      if (empty($this->WES_target)) {
          $input = str_replace("_INDEX_CHR_", "chr3", $ret1a[1]);
          $ret2a = $this->submit_stage_2b_step_2_g4($input . ".tbi", $ret1a[0], $sz);
      }else{
          $ret2a = $this->submit_stage_2b_step_2_g4($ret1a[1], $ret1a[0], $sz);
      }
      //
      // Step 3 - CollectInsertSizeMetrics
      $ret3 = $this->submit_stage_2b_step_3($ret0[1], $ret0[0], $sz);


      return $ret3;
    }

//
// individual steps
    function submit_stage_0_step_0( $fastq_map ){
        print("Submitting stage 0, step 0\n");

        $OUTPUT0 = "s3://wanglab-play/intermediates/$this->PRJ_ID/$this->SM/$this->RUN_ID/part0/$this->SM.ubam.bam";
        $sz1 = ceil($this->sz * 4);
        $threads = 8;
        $parameters = array(
            "SM" => $this->SM,
            "PRJ_ID" => $this->PRJ_ID,
            "ScriptName" => "stage0/docker/fastq2bam-docker.sh",
            "INFILE" => $fastq_map,
            "OUTFILE" => $OUTPUT0,
            "EXTRA" => "a",
        );

        $env = array(
            ['name' => 'MAX_THREADS', 'value' => $threads],
            ['name' => 'GETEBS', 'value' => 1],
            ['name' => 'EBSSIZE', 'value' => $sz1],
        );

        $ret = $this->submitJob("$this->RN-$this->SM-fastq2bam-step0", $threads, 16*1024, $parameters, $env);

        print_r($ret[0] . "\n\n");

        return [$ret[1], $OUTPUT0];
    }


    /**
     * read-name-sort
     * @param $rg
     * @param $input
     * @param int $sz
     * @param null $hold_job_id - possible dependent of submit_stage_0_step_0
     * @return array(submitted_job_id, output_filename]
     */
    function submit_stage_0_step_1($rg, $input, int $sz , $hold_job_id=null){
      print("Submitting stage 0, step 1\n");

      $OUTPUT1="s3://wanglab-play/intermediates/$this->PRJ_ID/$this->SM/_RID_/part1/$rg.sorted-byname.bam";
      $sz1 = ceil($sz * 4);
      if (substr($input,-5,5) == '.cram'){
          $sz1 += 8; // 8gb for reference sequence
      }
      $threads = 16;
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage0/docker/readname-sort-docker.sh",
        "INFILE" => $input,
        "OUTFILE" => $OUTPUT1,
        "EXTRA" => "RG=$rg",
      );

      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => 1],
        ['name' => 'EBSSIZE', 'value' => $sz1],
        ['name' => 'RG', 'value' => $rg],
      );

      $ret = $this->submitJob("$this->RN-$rg-step1",$threads, 10240, $parameters, $env, $hold_job_id);
      print_r($ret[0] . "\n\n");

      return [$ret[1], $OUTPUT1];
    }

    // check PE
    function submit_stage_0_step_2($rg, $input, $job_id){
      print("Submitting stage 0, step 2\n");
      $OUTPUT2 = "s3://wanglab-play/intermediates/$this->PRJ_ID/$this->SM/_RID_/part2/$rg.sorted-byname.bam";

      $threads = 1;
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage0/docker/check-paired-end-bam-docker.sh",
        "INFILE" => $input,
        "OUTFILE" => $OUTPUT2,
        "EXTRA" => "FORCE_RUN=0",
      );
      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => '1'],
      );

      $ret = $this->submitJob("$this->RN-$rg-step2", $threads, 1024, $parameters, $env,
        [array('jobId' => $job_id)]
      );
      print_r($ret[0] . "\n\n");

      return [$ret[1], $OUTPUT2];
    }

    // bwa-sort-merge
    function submit_stage_0_step_3($rg, $input, $job_id, int $sz){
      /*
       * Memory requirements include bwa-mem (~9GB, hg38), sort (parallel, 13GB+),
       * and sambamba merge
       */

      print("Submitting stage 0, step 3\n");
      $OUTPUT3 = "s3://wanglab-play/intermediates/$this->PRJ_ID/$this->SM/_RID_/part3/$this->SM.$rg.hg38-mapped.chr-merged.bam";
      $threads = 32;
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage0/docker/bwa-merge-docker.sh",
        "INFILE" => $input,
        "OUTFILE" => $OUTPUT3,
        "EXTRA" => "1 1 2 3 5",
      );

      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => 1],
        ['name' => 'EBSSIZE', 'value' => ceil(($sz*6)+9)],
      );

      $ret = $this->submitJob("$this->RN-$rg-step3-bwa-sort-merge", $threads, 58000, // minimal 16GB
        $parameters, $env,
        [array('jobId' => $job_id)]
      );
      print_r($ret[0] . "\n\n");
      return [$ret[1], $OUTPUT3];
    }

    function submit_stage_0_step_4($inputs, $job_ids){
      print("Submitting stage 0, step 4\n");
      $OUTPUT4 = "s3://wanglab-play/intermediates/$this->PRJ_ID/$this->SM/_RID_/part4/$this->SM.rg-merged.hg38.bam";
      $threads = 8;
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage1/docker/read-group-bams-merge-docker.sh",
        "INFILE" => join(' ', $inputs),
        "OUTFILE" => $OUTPUT4,
        "EXTRA" => "e f g",
      );
      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => 1],
        //['name' => 'EBSSIZE', 'value' => 100],
      );

      $ret = $this->submitJob("$this->RN-$this->SM-stage0-step4-mergeAll",$threads, 1024, $parameters, $env, $job_ids);
      print_r($ret[0] . "\n\n");
      return [$ret[1], $OUTPUT4];
    }

    // bamUtil markDup
    function submit_stage_1_step_1($input, $job_id){
      print("Submitting stage 1, step 1\n");

      try {
        $OUTPUT5 = "s3://wanglab-play/intermediates/$this->PRJ_ID/$this->SM/_RID_/part5/$this->SM.sorted.dupmarked.bam";
        $threads = 1;
        $parameters = array(
          "SM" => $this->SM,
          "PRJ_ID" => $this->PRJ_ID,
          "ScriptName" => "stage1/docker/bamutil-markdup-docker.sh",
          "INFILE" => $input,
          "OUTFILE" => $OUTPUT5,
          "EXTRA" => "d f g h",
        );

        $env = array(
          ['name' => 'MAX_THREADS', 'value' => $threads],
          ['name' => 'GETEBS', 'value' => 1],
          //['name' => 'EBSSIZE', 'value' => 100],
        );

        $ret = $this->submitJob("$this->RN-$this->SM-stage1-step1-markDup", $threads, 2048, $parameters, $env,
          [array('jobId' => $job_id)]
        );
        print_r($ret[0] . "\n\n");
      } catch (Aws\Exception\AwsException $e) {
        //echo $e->getAwsRequestId() . "\n";
        //echo $e->getAwsErrorType() . "\n";
        //echo $e->getAwsErrorCode() . "\n";
        $this->container->logger->addError($e->getMessage());
        print($e->getMessage());
        //return $response->withStatus(512, "Unknown Error");
        //exit(1);
        return [];
      }

      return [$ret[1], $OUTPUT5];
    }

    // DoC
    function submit_stage_1_step_2($input, $job_id, int $sz): array {
      print("Submitting stage 1, step 2\n");

      $extra = "a=1";
      $script_name = "stage1/docker/depthOfCoverage-docker.sh";
      $OUTPUT6 = "$this->rpath/$this->SM.depth.txt";
      $memory = 1024;
        $threads = 8;
      if (!empty($this->WES_target)){
          $extra = "TARGET=$this->WES_target";
          $script_name = "stage1/docker/depthOfCoverage-wes-docker.sh";
          $OUTPUT6 = "$this->rpath/$this->SM.depth.sample_summary";
          $threads = 1;
          $memory = 1024 * 10;
      }

      try {

        $parameters = array(
          "SM" => $this->SM,
          "PRJ_ID" => $this->PRJ_ID,
          "ScriptName" => $script_name,
          "INFILE" => $input,
          "OUTFILE" => $OUTPUT6,
          "EXTRA" => $extra,
        );

        $env = array(
          ['name' => 'MAX_THREADS', 'value' => $threads],
          ['name' => 'GETEBS', 'value' => '1'],
          ['name' => 'EBSSIZE', 'value' => ceil($sz  * 2) + 10],
        );

        $ret = $this->submitJob("$this->RN-$this->SM-stage1-step2-depthOfCoverage", $threads, $memory, $parameters, $env,
          [array('jobId' => $job_id)]
        );

        print_r($ret[0] . "\n\n");
      } catch (Aws\Exception\AwsException $e) {
        //echo $e->getAwsRequestId() . "\n";
        //echo $e->getAwsErrorType() . "\n";
        //echo $e->getAwsErrorCode() . "\n";
        $this->container->logger->addError($e->getMessage());
        print($e->getMessage());
        //return $response->withStatus(512, "Unknown Error");
        return [];
      }

      return [$ret[1], $OUTPUT6];
    }

    // BCal
    function submit_stage_2a_step_1a($input, $job_id, int $sz): array {
      print("Submitting stage 2a, step 1a (BCal)\n");

      $OUTPUT6 = "$this->rpath/$this->SM.recal_data.table";
      $threads = 12;
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage2a/docker/submit-bcal-docker.sh",
        "INFILE" => $input,
        "OUTFILE" => $OUTPUT6,
        "EXTRA" => "a b c d",
      );

      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => '1'],
        ['name' => 'EBSSIZE', 'value' => ceil(($sz+10) + 2)],
      );

      $ret = $this->submitJob("$this->RN-$this->SM-stage2a-step1a-bcal", $threads, 8192, $parameters, $env, //4608
        [array('jobId' => $job_id)]
      );

      print_r($ret[0] . "\n\n");

      return [$ret[1], $OUTPUT6];
    }

    // RTC
    function submit_stage_2a_step_1b($input, $job_id, int $sz): array {
      print("Submitting stage 2a, step 1b (RTC)\n");

      $OUTPUT7 = "$this->rpath/indels.$this->SM.intervals";
      $threads = 14;
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage2a/docker/submit-rtc-docker.sh",
        "INFILE" => $input,
        "OUTFILE" => $OUTPUT7,
        "EXTRA" => "a b c d",
      );

      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => '1'],
        ['name' => 'EBSSIZE', 'value' => ceil(($sz+10) + 2)],
      );

      $ret = $this->submitJob("$this->RN-$this->SM-stage2a-step1b-rtc", $threads, 20480, $parameters, $env,
        [array('jobId' => $job_id)]
      );

      print_r($ret[0] . "\n\n");


      return [$ret[1], $OUTPUT7];
    }

    //indelRealigner
    function submit_stage_2a_step_2($input, array $job_id, int $sz): array {
      print("Submitting stage 2a, step 2 (INDEL REALIGNMENT + MERGE)\n");

      $OUTPUT8 = "$this->rpath/$this->SM.hg38.realign.bqsr.bam";
      $threads = 8;
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage2a/docker/submit-indelRealigner-docker.sh",
        "INFILE" => $input[0],
        "OUTFILE" => $OUTPUT8,
        "EXTRA" => "BQSR=$input[1] REGIONS=$input[2]",
      );

      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => '1'],
        ['name' => 'EBSSIZE', 'value' => ceil(($sz+10) * 3)],
      );

      $ret = $this->submitJob("$this->RN-$this->SM-stage2a-step2-indelRealign", $threads, (12*1024*$threads) + 1024, $parameters, $env, $job_id);

      print_r($ret[0] . "\n\n");


      return [$ret[1], $OUTPUT8];
    }

    // HC - gatk3
    function submit_stage_2b_step_1(string $input, $job_id, int $sz): array {
      print("Submitting stage 2b, step 1 (HC)\n");
      $gvcf_prefix = "$this->rpath/VCF/GATK/$this->SM.raw_variants.";
      $threads = 6;

      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => '1'],
        ['name' => 'EBSSIZE', 'value' => ceil(($sz + 10) * 2)],
      );


      $OUTPUT9 = "${gvcf_prefix}_INDEX_CHR_.g.vcf.gz";
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage2b/docker/submit-hc-docker.sh",
        "INFILE" => $input,
        "OUTFILE" => $OUTPUT9,
        "EXTRA" => 'CHR=$AWS_BATCH_JOB_ARRAY_INDEX PCR=',
      );

      $ret = $this->submitJob("$this->RN-$this->SM-stage2b-step1-HC", $threads, 20 * 1024, $parameters, $env,
        [array('jobId' => $job_id)],25
      );

      print_r($ret[0] . "\n");

      print("\n");
      return [$ret[1], $OUTPUT9];
    }

    // HC - GATK4.1
    function submit_stage_2b_step_1_g4(string $input, $job_id, int $sz): array {
        print("Submitting stage 2b, step 1 (HC-g4)\n");
        $gvcf_prefix = "$this->rpath/VCF/GATK4.1/$this->SM.raw_variants.";
        $threads = 8;

        $OUTPUT9 = "${gvcf_prefix}_INDEX_CHR_.g.vcf.gz";
        $script_name = "stage2b/docker/submit-hc-gatk4-docker.sh";
        $extra = 'CHR=$AWS_BATCH_JOB_ARRAY_INDEX';
        $array_size=25;
        if (!empty($this->WES_target)){
            $OUTPUT9 = "${gvcf_prefix}.g.vcf.gz";
            $script_name = "stage2b/docker/submit-hc-wes-gatk4-docker.sh";
            $extra = "TARGET=$this->WES_target";
            $array_size = NULL;
        }


        $parameters = array(
            "SM" => $this->SM,
            "PRJ_ID" => $this->PRJ_ID,
            "ScriptName" => $script_name,
            "INFILE" => $input,
            "OUTFILE" => $OUTPUT9,
            "EXTRA" => $extra,
        );

        $env = array(
            ['name' => 'MAX_THREADS', 'value' => $threads],
            ['name' => 'GETEBS', 'value' => '1'],
            ['name' => 'EBSSIZE', 'value' => ceil(($sz + 10) * 2)],
        );

        if ($job_id){
            $depends = [array('jobId' => $job_id)];
        }else{
            $depends = null;
        }

        $ret = $this->submitJob("$this->RN-$this->SM-stage2b-step1-HC-G4", $threads, 20 * 1024, $parameters, $env,
            $depends, $array_size
        );

        print_r($ret[0] . "\n");

        print("\n");
        return [$ret[1], $OUTPUT9];
    }

    // variantEval
    function submit_stage_2b_step_2(string $input, $job_id, int $sz): array {
      print("Submitting stage 2b, step 2 (Variant Eval)\n");
      $threads = 8;

      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => '1'],
        ['name' => 'EBSSIZE', 'value' => ceil(($sz + 10) * 2)],
      );


      $OUTPUT10 = "$this->rpath/eval.$this->SM.txt";
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage2b/docker/submit-variantEval-docker.sh",
        "INFILE" => $input,
        "OUTFILE" => $OUTPUT10,
        "EXTRA" => "a",
      );

      $ret = $this->submitJob("$this->RN-$this->SM-stage2b-step2-VariantEval", $threads, 30 * 1024, $parameters, $env,
        [array('jobId' => $job_id)]
      );

      print_r($ret[0] . "\n");

      print("\n");
      return [$ret[1], $OUTPUT10];
    }

    // variantEval - GATK4.1
    function submit_stage_2b_step_2_g4(string $input, $job_id, int $sz): array {
        print("Submitting stage 2b, step 2 (Variant Eval)\n");
        $threads = 8;

        $script_name = "stage2b/docker/submit-variantEval-gatk4-docker.sh";
        $extra = "a";
        if (!empty($this->WES_target)) {
            $script_name = "stage2b/docker/submit-variantEval-wes-gatk4-docker.sh";
            $extra = "TARGET=$this->WES_target";
        }

        $OUTPUT10 = "$this->rpath/eval.gatk4.$this->SM.txt";
        $parameters = array(
            "SM" => $this->SM,
            "PRJ_ID" => $this->PRJ_ID,
            "ScriptName" => $script_name,
            "INFILE" => $input,
            "OUTFILE" => $OUTPUT10,
            "EXTRA" => $extra,
        );
        $env = array(
            ['name' => 'MAX_THREADS', 'value' => $threads],
            ['name' => 'GETEBS', 'value' => '1'],
            ['name' => 'EBSSIZE', 'value' => ceil(($sz + 10) * 2)],
        );

        $ret = $this->submitJob("$this->RN-$this->SM-stage2b-step2-VariantEval-G4", $threads, 30 * 1024, $parameters, $env,
            [array('jobId' => $job_id)]
        );

        print_r($ret[0] . "\n");

        print("\n");
        return [$ret[1], $OUTPUT10];
    }

    // CollectInsertSize
    function submit_stage_2b_step_3(string $input, $job_id, int $sz): array {
      print("Submitting stage 2b, step 3 (CollectInsertSize)\n");
      $threads = 1;

      $env = array(
        ['name' => 'MAX_THREADS', 'value' => $threads],
        ['name' => 'GETEBS', 'value' => '1'],
        ['name' => 'EBSSIZE', 'value' => ceil(($sz) * 2)],
      );

      $input = str_replace('.cram', '.bam', $input);
      $OUTPUT10 = "$this->rpath/$this->SM-insert_size_metric.tsv";
      $parameters = array(
        "SM" => $this->SM,
        "PRJ_ID" => $this->PRJ_ID,
        "ScriptName" => "stage2b/docker/submit-CollectInsertSize-docker.sh",
        "INFILE" => $input,
        "OUTFILE" => $OUTPUT10,
        "EXTRA" => "a",
      );

      if ($job_id){
          $depends = [array('jobId' => $job_id)];
      }else{
          $depends = null;
      }

      $ret = $this->submitJob("$this->RN-$this->SM-stage2b-step3-ISz", $threads, 2 * 1024, $parameters, $env,
          $depends
      );

      print_r($ret[0] . "\n");

      print("\n");
      return [$ret[1], $OUTPUT10];
    }

  }
