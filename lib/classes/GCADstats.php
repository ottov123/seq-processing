<?php

/**
 * User: otto
 * Date: 12/17/16
 */
use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class GCADstats
{
    protected $aws_ec2;
    protected $aws_cw;
    protected $view;
    protected $db;

    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $c The data to use to create
     */
    public function __construct(ContainerInterface $c)
    {
        $this->aws_ec2 = $c->aws_ec2;
        $this->aws_cw = $c->aws_cw;
        $this->view = $c->view;
        $this->db = $c->db;
    }

    protected function render(ResponseInterface $response, $view, $data = [])
    {
        /* @var $renderer \Slim\Views\PhpRenderer */
        //$renderer = $this->container->get('renderer');
        //$templateFinder = $this->container->get('templateFinder');
        //return $renderer->render($response, $templateFinder($view), $data);
        return $this->view->render($response, $view, $data);
    }

    /**
     *  getGCAD_instances - querys EC2 for account's instance list, sorts by launch time
     *  @return array of Reservations (instances)
     */
    public function getGCAD_instances()
    {

        // filter by AMI list in .env
        if (!empty(getenv("AWS_AMI_FILTER"))){
          $ami_list = explode(',', getenv("AWS_AMI_FILTER"));
          $ami_filter = array('Name' => 'image-id',
                            'Values' => $ami_list
                             );
        }else{
          $ami_filter = array();
        }

        // optional filter by instance-type
        $ins_filter = array('Name'=>'instance-type','Values' => array('r3.8xlarge'));

        // optionally clear filers
        //$ami_filter = array();
        $ins_filter = array();

        // aws ec2 describe-instances
        $result = $this->aws_ec2->describeInstances([
            'Filters' => [
                $ami_filter,
                $ins_filter,
            ],
        ]);

        // Sort by LaunchTime
        usort($result['Reservations'], function ($a, $b) {
            return $b['Instances'][0]['LaunchTime'] <=> $a['Instances'][0]['LaunchTime'];
        });

        return $result;
    }

    /**
     * @return mixed
     */
    public function getInstanceCPU_history($ID, $quant, $start, $end)
    {
        // query cpu metrics for this instance-id ($ID)
        $stat = $this->aws_cw->getMetricStatistics([
            'Namespace'  => 'AWS/EC2',
            'MetricName' => 'CPUUtilization',
            'Period'     => $quant,
            'Statistics' => ['Maximum'],
            'StartTime'  => $start,
            'EndTime'    => $end,
            'Dimensions' => [
                [
                    'Name'  => 'InstanceId',
                    'Value' => $ID,
                ],
            ],
        ]);

        usort($stat[ 'Datapoints' ], function($a, $b) {
            return $b['Timestamp'] <=> $a['Timestamp'];
        });

        return $stat;
    }

    /**
     * linked to route /v1/running
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function viewRunning(Request $request, Response $response, $args)
    {
        $iList = $this->getGCAD_instances();

        // database data for single instance, prepared statement
        $sql = "SELECT SEQV.tracking_value AS operation, SM.*, PRJ.*
                    FROM seq_tracking_processing_steps_VARCHAR AS SEQV
                    INNER JOIN seq_runs AS RN ON SEQV.run_id = RN.ID
                    INNER JOIN seq_samples  AS SM  ON RN.sample_id = SM.sample_id
                    INNER JOIN seq_projects AS PRJ ON RN.project_id = PRJ.project_id

                    WHERE SEQV.tracking_step_id = 100
                        AND RN.ID = (SELECT MAX(ID) FROM seq_runs RN2 WHERE RN2.project_id = SM.project_id AND RN2.sample_id = SM.sample_id)
                        AND SM.sample_name = :sm
                        AND SM.project_id = :pid";

        $stmt = $this->db->prepare($sql);

        //
        $this->render($response,
                 'table_running.phtml',
                    ['result' => $iList,
                        'stmt' => $stmt,
                        'fun' => array($this,'getInstanceCPU_history'),
                    ]);
    }
    /**
     * linked to route /v1/running2
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function viewRunning2(Request $request, Response $response, $args)
    {
        $iList = $this->getGCAD_instances();

        // database data for single instance, prepared statement
        $sql = "SELECT SEQV.tracking_value AS operation, SM.*, PRJ.*
                    FROM seq_tracking_processing_steps_VARCHAR AS SEQV
                    INNER JOIN seq_runs AS RN ON SEQV.run_id = RN.ID
                    INNER JOIN seq_samples  AS SM  ON RN.sample_id = SM.sample_id
                    INNER JOIN seq_projects AS PRJ ON RN.project_id = PRJ.project_id

                    WHERE SEQV.tracking_step_id = 100
                        AND RN.ID = (SELECT MAX(ID) FROM seq_runs RN2 WHERE RN2.project_id = SM.project_id AND RN2.sample_id = SM.sample_id)
                        AND SM.sample_name = :sm
                        AND SM.project_id = :pid";

        $stmt = $this->db->prepare($sql);
print "<pre>";
$k1=array_keys($iList[ 'Reservations' ]);
$k2=array_pop($k1);
unset($iList['Reservations'][$k1]);
print_r($iList['Reservations']);
        //
        $this->render($response,
                 'table_running.phtml',
                    ['result' => $iList[0],
                        'stmt' => $stmt,
                        'fun' => array($this,'getInstanceCPU_history'),
                    ]);
    }

}
