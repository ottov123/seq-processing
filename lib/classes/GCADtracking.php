<?php

/**
 * User: otto
 * Date: 12/17/16
 */
use Slim\Http\Request;
use Slim\Http\Response;
use Interop\Container\ContainerInterface;

class GCADtracking
{
    protected $container;

    // constructor receives container instance
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function validateProjectID(int $project_id){
        // Validate project_id
        $sql_project = "SELECT project_id
                        FROM seq_projects
                        WHERE project_id = :project_id
                        ";
        $stmt = $this->container->db->prepare($sql_project);
        $stmt->execute(array( ":project_id" => $project_id ));
        return $stmt->rowCount();
    }
    public function validateSampleID(int $sample_id){
        // Validate sample_id
        $sql_sample = "SELECT sample_id
                       FROM seq_samples
                       WHERE sample_id = :sample_id
                       ";
        $stmt = $this->container->db->prepare($sql_sample);
        $stmt->execute(array( ":sample_id" => $sample_id ));
        return $stmt->rowCount();
    }
    public function validateTrackingID(int $tracking_id){
        $sql_track = "SELECT track_step_id
                      FROM seq_tracking_processing_steps
                      WHERE track_step_id = :tracking_id
                      ";
        $stmt = $this->container->db->prepare($sql_track);
        $stmt->execute(array( ":tracking_id" => $tracking_id ));
        return $stmt->rowCount();
    }
    public function validateSampleName(string $sample_name, int $project_id){
        // Validate sample_name
        $sql_sample = "SELECT sample_id
                   FROM seq_samples
                   WHERE sample_name = :sample_name
                      AND project_id = :project_id
                   ";
        $stmt = $this->container->db->prepare($sql_sample);
        $stmt->execute(array( ":sample_name" => $sample_name, ":project_id" => $project_id ));
        //$count = $stmt->rowCount();
        $sample_id = $stmt->fetchColumn(0);
        return $sample_id;
    }
    public function validateTrackingName(string $tracking_name){
        // Validate tracking name
        $sql_track = "SELECT track_step_id
                  FROM seq_tracking_processing_steps
                  WHERE step_name_short = :tracking_name
                  ";
        $stmt = $this->container->db->prepare($sql_track);
        $stmt->execute(array( ":tracking_name" => $tracking_name ));
        //$count = $stmt->rowCount();
        //if ($count < 1){ jsonFailMsg($response, "tracking_name not found");return;}
        $result = $stmt->fetch();
        return $result['track_step_id'];
    }
    public function validateRunID(int $run_id) {
        // Validate tracking name
        $sql_track = "SELECT MAX(ID) AS MX, project_id, sample_id
                      FROM seq_runs
                      WHERE ID = :run_id
                      GROUP BY project_id, sample_id
                      ";
        $stmt = $this->container->db->prepare($sql_track);
        $stmt->execute(array( ":run_id" => $run_id ));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Extract attr_type to determine tbl name
     */
    public function getTblType(int $tracking_id){
        $sql_track = "SELECT step_type
                      FROM seq_tracking_processing_steps
                      WHERE track_step_id = :tracking_id
                      ";
        $stmt = $this->container->db->prepare($sql_track);
        $stmt->execute(array( ":tracking_id" => $tracking_id ));
        $row = $stmt->fetch();
        return $row['step_type'];
    }

    public function insertTrackingAttribute(int $project_id, int $sample_id, int $tracking_id, string $val){
        $count = $this->validateProjectID($project_id);
        if ($count != 1) { return jsonFormatMsg("project_id not valid");}

        $count = $this->validateSampleID($sample_id);
        if ($count < 1){ return jsonFormatMsg("sample_id not found");}
        if ($count > 1){ return jsonFormatMsg("multiple sample_id not supported");}

        $count = $this->validateTrackingID($tracking_id);
        if ($count != 1) { return jsonFormatMsg("project_id not valid");}

        $tbl_type = $this->getTblType($tracking_id);

        $sql = "INSERT INTO
            seq_tracking_processing_steps_$tbl_type
            (project_id, sample_id, tracking_step_id, tracking_value) VALUES
            (:project_id, :sample_id, :tracking_id, :val)
            ON DUPLICATE KEY UPDATE tracking_value = :val
           ";
        $stmt = $this->container->db->prepare($sql);

        try {

            $result = $stmt->execute(
                array(
                    ':project_id' => $project_id,
                    ':sample_id'  => $sample_id,
                    ':tracking_id' => $tracking_id,
                    ':val'         => $val)
            );
            $json = array('status' => 'success', 'id' => $this->container->db->lastInsertId() );
            return $json;

        } catch( PDOException $Exception) {

            if ($Exception->getCode() == 23000){
                $json = array('status' => 'failed', 'message' => 'Duplicate','errCode' => $Exception->getCode() );
                return $json;
            }
            $this->container->logger->addInfo("Adding a tracking value error:" . $Exception->getMessage() );
            $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
            return $json;
        }
    }
    public function insertRunTrackingAttribute(int $run_id, int $tracking_id, string $val){

        $count = $this->validateTrackingID($tracking_id);
        if ($count != 1) { return jsonFormatMsg("project_id not valid");}

        $tbl_type = $this->getTblType($tracking_id);

        $sql = "INSERT INTO
            seq_tracking_processing_steps_$tbl_type
            (run_id, tracking_step_id, tracking_value) VALUES
            (:run_id, :tracking_id, :val)
            ON DUPLICATE KEY UPDATE tracking_value = :val
           ";
        $stmt = $this->container->db->prepare($sql);

        try {

            $result = $stmt->execute(
                array(
                    ':run_id' => $run_id,
                    ':tracking_id' => $tracking_id,
                    ':val'         => $val)
            );
            $json = array('status' => 'success', 'id' => $this->container->db->lastInsertId() );
            return $json;

        } catch( PDOException $Exception) {

            if ($Exception->getCode() == 23000){
                $json = array('status' => 'failed', 'message' => 'Duplicate','errCode' => $Exception->getCode() );
                return $json;
            }
            $this->container->logger->addInfo("Adding a tracking value error:" . $Exception->getMessage() );
            $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
            return $json;
        }
    }
    /**
     * linked to route /v1/track/by-id/{sample_id}/{project_id}/{tracking_id}/{value}
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function trackById(Request $request, Response $response, $args)
    {
        if (count($args)<>4){
            jsonFailMsg($response, "callback requires sample_id, project_id, tracking_id, and value");
        }

        $sample_id = filter_var($args['sample_id'], FILTER_SANITIZE_NUMBER_INT);
        $project_id = filter_var($args['project_id'], FILTER_SANITIZE_NUMBER_INT);
        $tracking_id = filter_var($args['tracking_id'], FILTER_SANITIZE_NUMBER_INT);
        $value = rawurldecode(filter_var($args['value'], FILTER_SANITIZE_ENCODED));

        $json = $this->insertTrackingAttribute($project_id, $sample_id, $tracking_id, $value);
        return $response->withJson($json);
    }

    /**
     * linked to route /v1/track/by-name/{sample_name}/{project_id}/{tracking_name}/{value}
     * store a tracking attribute for a sample, e.g. current_operation
     */
    public function trackByName(Request $request, Response $response, $args)
    {
        if (count($args)<>4){
            jsonFailMsg($response, "callback requires sample_name, project_id, tracking_name, and value");
        }

        $sample_name = filter_var($args['sample_name'], FILTER_SANITIZE_STRING);
        $project_id = filter_var($args['project_id'], FILTER_SANITIZE_NUMBER_INT);
        $tracking_name = filter_var($args['tracking_name'], FILTER_SANITIZE_STRING);
        $value = rawurldecode(filter_var($args['value'], FILTER_SANITIZE_ENCODED));

        $sample_id = $this->validateSampleName($sample_name, $project_id);
        if (empty($sample_id)){ return jsonFailMsg($response, "sample_name not found");}
        $tracking_id = $this->validateTrackingName($tracking_name);
        if (empty($tracking_id)){ return jsonFailMsg($response, "tracking_name not found");}

        $json = $this->insertTrackingAttribute($project_id, $sample_id, $tracking_id, $value);
        return $response->withJson($json);
    }

    /**
     * linked to route /v1/track/by-run/{run_id}/{tracking_name}/{value}
     * store a tracking attribute for a sample, e.g. current_operation
     */
    public function trackByRun(Request $request, Response $response, $args)
    {
        if (count($args)<>4){
            jsonFailMsg($response, "callback requires sample_name, project_id, tracking_name, and value");
        }

        $run_id = filter_var($args['run_id'], FILTER_SANITIZE_NUMBER_INT);
        $tracking_name = filter_var($args['tracking_name'], FILTER_SANITIZE_STRING);
        $value = rawurldecode(filter_var($args['value'], FILTER_SANITIZE_ENCODED));

        $rData = $this->validateRunID($run_id);
        if (count($rData)<1){return jsonFailMsg($response, "run_id not found");}
        //$sample_id = $rData['sample_id'];
        //$project_id = $rData['project_id'];

        $tracking_id = $this->validateTrackingName($tracking_name);
        if (empty($tracking_id)){ return jsonFailMsg($response, "tracking_name not found");}

        $json = $this->insertRunTrackingAttribute($run_id, $tracking_id, $value);
        return $response->withJson($json);
    }

    // function for route: /v1/track/init-run/{project_id}/{sample_name}
    public function initRun(Request $request, Response $response, $args){
      if (count($args)<>2){
        jsonFailMsg($response, "callback requires sample_name and project_id");
      }

      $project_id = filter_var($args['project_id'], FILTER_SANITIZE_NUMBER_INT);
      $sample_name = filter_var($args['sample_name'], FILTER_SANITIZE_STRING);

      $count = $this->validateProjectID($project_id);
      if ($count != 1) { return jsonFailMsg($response, "project_id not valid");}

      $sample_id = $this->validateSampleName($sample_name, $project_id);
      if (empty($sample_id)){ return jsonFailMsg($response, "sample_name not found");}

      $sql = "INSERT INTO
              seq_runs
              (project_id, sample_id) VALUES
              (:project_id, :sample_id)
             ";
      $stmt = $this->container->db->prepare($sql);

      try {

        $result = $stmt->execute(
          array(
            ':project_id' => $project_id,
            ':sample_id'  => $sample_id
          )
        );
        $json = array('status' => 'success', 'id' => $this->container->db->lastInsertId() );

      } catch( PDOException $Exception) {
        $this->container->logger->addInfo("Adding run error:" . $Exception->getMessage() );
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );

      } catch( Exception $Exception) {
        $this->container->logger->addInfo("Adding run error:" . $Exception->getMessage() );
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );

      }

      return $response->withJson($json);
    }
    // function for route: /v1/track/get-run/{project_id}/{sample_name}
    // returns the latest run_id
    public function getRun(Request $request, Response $response, $args){
      if (count($args)<>2){
        jsonFailMsg($response, "callback requires sample_name and project_id");
      }

      $project_id = filter_var($args['project_id'], FILTER_SANITIZE_NUMBER_INT);
      $sample_name = filter_var($args['sample_name'], FILTER_SANITIZE_STRING);

      $count = $this->validateProjectID($project_id);
      if ($count != 1) { return jsonFailMsg($response, "project_id not valid");}

      $sample_id = $this->validateSampleName($sample_name, $project_id);
      if (empty($sample_id)){ return jsonFailMsg($response, "sample_name not found");}

      $sql = "SELECT MAX(ID) AS MX
                      FROM seq_runs
                      WHERE project_id = :project_id
                         AND sample_id = :sample_id
                      ";
      $stmt = $this->container->db->prepare($sql);

      try {

        $stmt->execute(
          array(
            ':project_id' => $project_id,
            ':sample_id'  => $sample_id
          )
        );
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $json = array('status' => 'success', 'id' => $result['MX'] );

      } catch( PDOException $Exception) {
        $this->container->logger->addInfo("Adding run error:" . $Exception->getMessage() );
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );

      } catch( Exception $Exception) {
        $this->container->logger->addInfo("Adding run error:" . $Exception->getMessage() );
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );

      }

      return $response->withJson($json);
    }
}
