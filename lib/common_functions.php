<?php


/**
 *
 */
function tablePrintQueryResult($stmt){

  // Get table headers
  $numfields = $stmt->columnCount();

  echo "<table border=1 cellpadding=5>\n<tr>";

  for ($i=0; $i < $numfields; $i++) {
   echo '<th>';
   echo $stmt->getColumnMeta($i)['name'];
   echo '</th>';
  }
  echo "</tr>\n";

  // Get table data
  while ($row = $stmt->fetch()) {
    echo '<tr><td>';

    echo implode($row,'</td><td>');
    echo "</td></tr>\n";
  }
  echo "</table>\n";



}

function jsonFailMsg($response, $msg){

 $json = array('status' => 'failed', 'message' => $msg );
 return $response->withJson($json);

}
function jsonFormatMsg($msg){

    $json = array('status' => 'failed', 'message' => $msg );
    return $json;

}

/**
 * A sweet interval formatting, will use the two biggest interval parts.
 * On small intervals, you get minutes and seconds.
 * On big intervals, you get months and days.
 * Only the two biggest parts are used.
 *
 * @param DateTime $start
 * @param DateTime|null $end
 * @return string
 */
function formatDateDiff($start, $end=null) {
    if(!($start instanceof DateTime)) {
        $start = new DateTime($start);
    }

    if($end === null) {
        $end = new DateTime();
    }

    if(!($end instanceof DateTime)) {
        $end = new DateTime($start);
    }

    $interval = $end->diff($start);
    $doPlural = function($nb,$str){return $nb>1?$str.'s':$str;}; // adds plurals

    $format = array();
    if($interval->y !== 0) {
        $format[] = "%y ".$doPlural($interval->y, "yr");
    }
    if($interval->m !== 0) {
        $format[] = "%m ".$doPlural($interval->m, "month");
    }
    if($interval->d !== 0) {
        $format[] = "%d ".$doPlural($interval->d, "dy");
    }
    if($interval->h !== 0) {
        $format[] = "%h ".$doPlural($interval->h, "hr");
    }
    if($interval->i !== 0) {
        $format[] = "%i ".$doPlural($interval->i, "min");
    }
    if($interval->s !== 0) {
        if(!count($format)) {
            return "less than a minute ago";
        } else {
            $format[] = "%s ".$doPlural($interval->s, "second");
        }
    }

    // We use the two biggest parts
    if(count($format) > 1) {
        $format = array_shift($format)." and ".array_shift($format);
    } else {
        $format = array_pop($format);
    }

    // Prepend 'since ' or whatever you like
    return $interval->format($format);
}
