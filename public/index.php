<?php
/*
 * Copyright (c) 2016 Otto Valladares
 *
 *
 * Project home:
 *   https://bitbucket.org/ottov123/seq-processing
 *
 */
//use \Psr\Http\Message\ServerRequestInterface as Request;
//use \Psr\Http\Message\ResponseInterface as Response;
use Pimple\ServiceProviderInterface;
use Pimple\Container;
defined('APP_PATH')  || define('APP_PATH',  dirname(__DIR__));
defined('ROOT_PATH') || define('ROOT_PATH', APP_PATH);
defined('CONF_PATH') || define('CONF_PATH', ROOT_PATH . '/config');
defined('TMP_PATH')  || define('TMP_PATH',  ROOT_PATH . '/tmp');
defined('LOG_PATH')  || define('LOG_PATH',  ROOT_PATH . '/logs');
defined('CACHE_PATH')|| define('CACHE_PATH',ROOT_PATH . '/tmp/cache');
defined('VIEW_PATH') || define('VIEW_PATH', ROOT_PATH . '/templates');

// Load all classes
require_once ROOT_PATH . '/vendor/autoload.php';

spl_autoload_register(function ($classname) {
    require (ROOT_PATH . "/lib/classes/" . $classname . ".php");
});

// Load env
// provides getenv()
$dotenv = new Dotenv\Dotenv(ROOT_PATH);
$dotenv->load();


// Initialize
// Create new application
$app = new \Slim\App([
    "settings" => [
        "displayErrorDetails" => true,
        "addContentLengthHeader" => false,
    ]
]);

//  Load primary dependency container
$container = $app->getContainer();

// Add dependencies
$container['view'] = new \Slim\Views\PhpRenderer(VIEW_PATH);

$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler(LOG_PATH . "/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};

require_once CONF_PATH . "/database.php";
require_once CONF_PATH . "/aws.php";
require_once CONF_PATH . "/handlers.php";
require_once ROOT_PATH . "/lib/common_functions.php";

$app->get("/", function ($request, $response, $arguments) {
    print "doh!";
});


// Load routes
require_once ROOT_PATH . "/routes/projects.php";
require_once ROOT_PATH . "/routes/sample.php";
require_once ROOT_PATH . "/routes/track.php";
require_once ROOT_PATH . "/routes/views.php";
require_once ROOT_PATH . "/routes/run.php";
require_once ROOT_PATH . "/routes/qc.php";

require_once ROOT_PATH . "/routes/batch.php";

$app->get('/test',function ($request,$response,$arg) {
    $response = $response->withStatus(419,'Authentication Timed Out');
    return $response;
});


$app->get('/test-db',function ($request,$response,$arg) {
  try{
   $stmt = $this->db->prepare("SELECT 1");
   $data = $stmt->fetchAll();
   print_r($data);
  } catch (PDOException $Exception) {
        $this->logger->addInfo("Failed db test");
        $json = array('status' => 'failed',
                      'message' => $Exception->getMessage(),
                      'errCode' => $Exception->getCode(),
                      'DB_HOST' => getenv("DB_HOST"),
                      'dbname'  => getenv("DB_NAME"),
                      'DB_USER' => getenv("DB_USER"),
                      'DB_PASSWORD' => getenv("DB_PASSWORD"),
                 );
        return $response->withJson($json,200);
  }
});


/**
 * Run application.
 */
$app->run();
