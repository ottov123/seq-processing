<?php

/**
 * print seq_project table
 * @var rettype return type for response
 */
$app->get("/v1/projects", \GCAD_projects::class . ':listProjects')
    ->setName('project-list');


/**
 * Lookup project
 */
$app->get("/v1/projects/{name}", \GCAD_projects::class . ':lookupProject')
    ->setName('project-detail');


/**
 * Add project
 * @var required project_name
 */
$app->post("/v1/projects/add", \GCAD_projects::class . ':addProject')
  ->setName('project-add');


/**
 * var required project_id, sample_name,
 * var optional subject_name, LSAC, seq_type {WES/WGS}
 */
$app->post("/v1/projects/sample/add", \GCAD_projects::class . ':addProjectSample')
  ->setName('project-add-sample');

/**
 * list samples
 * @var rettype
 */
$app->get("/v1/projects/samples/by-project-id/{id}", \GCAD_projects::class . ':getSamplesByPid')
  ->setName('project-id-samples');

$app->get("/v1/projects/samples/by-project-name/{name}", function ($request, $response, $arg) {

});

$app->get("/v1/projects/get-attr/{column_name}", function ($request, $response, $args) {
    $q = $request->getQueryParams("project_id");
    if (empty($q['project_id'])){print "missing project_id";exit();}
    $pid = filter_var($q['project_id'], FILTER_SANITIZE_NUMBER_INT);
    $col = filter_var($args['column_name'], FILTER_SANITIZE_STRING);

    $sql = "SELECT `$col`
            FROM seq_projects
            WHERE project_id = :pid
           ";

    $stmt = $this->db->prepare($sql);

    try{
        $stmt->execute(array(
                ":pid" => (int)$pid
            )
        );

        $result = $stmt->fetchColumn();
        //print($result);

        $json = array('status' => 'success', 'value' => $result );
        return $response->withJson($json,200);
    } catch (PDOException $Exception) {
        $this->logger->addInfo("Failed project-sample lookup by id");
        $json = array('status' => 'failed', 'message' => $Exception->getMessage(),'errCode' => $Exception->getCode() );
        return $response->withJson($json,200);
    } catch( Exception $e ){
      print_r($e);
    }
});
