<?php
/**
 *
 *
 *
 */

/**
 * convert sample name to id
 */
$app->get("/v1/sample/sample-name/{name}", \GCAD_samples::class . ':getSampleIdByName');

$app->get("/v1/sample/set-attr/{attr_name}/{project_id}/{sample_name}/{value}", \GCAD_samples::class . ':setSampleAttribute');
$app->get("/v1/sample/set-attr-by-id/{attr_name}/{project_id}/{sample_id}/{value}", \GCAD_samples::class . ':setSampleAttributeById');

$app->get("/v1/sample/get-attr/{attr_name}/{project_id}/{sample_name}", \GCAD_samples::class . ':getSampleAttribute');

$app->get("/v1/sample/details", \GCAD_samples::class . ':getSampleDetails');

/**
 * Get sample's target file (WES)
 */

$app->get("/v1/sample/get-target/{project_id}/{sample_name}", \GCAD_samples::class . ':getTargetFileLocation');

$app->post("/v1/sample/define-target", \GCAD_samples::class . ':defineTargetFileLocation');

