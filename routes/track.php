<?php
/*
 *
 *
 *
 */



$app->get("/v1/track[/{id}]", function ($request, $response, $args) {

    $whereClause = "";
    if (isset($args['id'])){
       if (is_numeric($args['id'])){
         $tracking_id = filter_var($args['id'], FILTER_SANITIZE_NUMBER_INT);
         $whereClause = "WHERE track_step_id = $tracking_id";
       } else {
         $step_name = filter_var($args['id'], FILTER_SANITIZE_STRING);
         $whereClause = "WHERE step_name_short = " . $this->db->quote($step_name);
       }
    }

    $this->logger->addInfo("Listing projects");

    $data = $request->getQueryParams();

    $sql = "SELECT *
            FROM seq_tracking_processing_steps AS T
            $whereClause
           ";

    $stmt = $this->db->query($sql);
    $count = $stmt->rowCount();
    if ($count < 1) { jsonFailMsg($response, "id not valid");return;}

    // default print to HTML
    if (isset($data['rettype'])){
      if ($data['rettype'] == "json"){
        $result = $stmt->fetchAll();
        return $response->withJson($result);
      } else {
       tablePrintQueryResult($stmt);
      }
    } else{
       tablePrintQueryResult($stmt);
   }

});

/**
 *
 */
//$app->get('/v1/track/by-id/{sample_id}/{project_id}/{tracking_id}/{value}', \GCADtracking::class . ':trackById')->setName('setById');
//$app->get('/v1/track/by-name/{sample_name}/{project_id}/{tracking_name}/{value}', \GCADtracking::class . ':trackByName')->setName('setByName');

$app->get('/v1/track/by-run/{run_id}/{tracking_name}/{value}', \GCADtracking::class . ':trackByRun')->setName('setByRun');
$app->get('/v1/track/init-run/{project_id}/{sample_name}', \GCADtracking::class . ':initRun')->setName('initRun');
$app->get('/v1/track/get-run/{project_id}/{sample_name}', \GCADtracking::class . ':getRun')->setName('getRun');

